var_resposta = None
respostas_possiveis = ("sim", "talvez", "nao" )
while var_resposta not in respostas_possiveis:
    var_verbos = input ("digite o verbo aqui: ")
    var_verbos = var_verbos.strip()
    print ("você quis dizer: {}?".format(var_verbos))
    var_resposta = input ("sim, talvez, nao: ")
    var_resposta = var_resposta.strip ()

    if var_resposta in respostas_possiveis:
        
        if var_resposta == "sim": 
            print ("ok")

        elif var_resposta == "talvez": 
            print ("porque tão indeciso? ")

        else:
            print ("qual verbo você quis dizer então? ")
            var_resposta = None
    else: 
        print ("a sua resposta deve ser uma das opções acima!")
     
        